var glob, objectAssign, path;

objectAssign = require('object-assign');
glob = require('glob');
path = require('path');

exports.register = function (plugin, options, next) {

  var defaults, file, modelName, thinky, _i, _len, _ref;

  defaults = {
    host: 'localhost',
    port: 28015,
    db: 'test',
    modelsDir: '/server/models'
  };

  options = objectAssign(defaults, options);
  thinky = require('thinky')(options);
  plugin.expose('thinky', thinky);
  plugin.expose('r', thinky.r);

  _ref = glob.sync(process.cwd() + options.modelsDir + "/*");

  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    file = _ref[_i];
    modelName = path.basename(file, path.extname(file));
    plugin.expose(modelName, require(file)(thinky, thinky.r));
  }

  plugin.log(['hapi-thinky', 'info'], 'Rethinkdb connection created');

  return next();
};

exports.register.attributes = {
  pkg: require('../package.json')
};