hapi-thinky
=====================

[Hapi](http://hapijs.com/) plugin that automatically loads your [Thinky](http://thinky.io) models.

## Install
`npm install hapi-thinky`

## Usage
Load plugin into your Hapi server as normal.

#### Register via manifest

```json
{
    "servers": [{
        "port": 8080
    }],
    "plugins": {
        "hapi-thinky": {
			host: 'localhost',
			port: 28015,
			db: 'test',
			modelsDir: '/models'
	  }
    }
}
```
